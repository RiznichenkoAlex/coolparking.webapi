﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionsService
    {
        List<TransactionInfo> LastTransactionInfo();

        string AllTransactions();

        Vehicle TopUpVehicle(ToUpVehicleRequest toUpVehicleRequest);
    }
}
