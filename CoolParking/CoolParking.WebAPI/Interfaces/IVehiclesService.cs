﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using System.Collections.ObjectModel;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehiclesService
    {
        ReadOnlyCollection<Vehicle> Vehicles();

        VehicleRequest VehicleById(string id);

        VehicleRequest AddVehicle(VehicleRequest vehicleRequest);

        void RemoveVehicleById(string id);
    }
}
