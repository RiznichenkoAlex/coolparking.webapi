﻿namespace CoolParking.WebAPI.Interfaces
{
    public interface IParkingService
    {
        decimal Balance();

        int Capacity();

        int FreePlaces();
    }
}
