﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Exceptions;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using IParkingService = CoolParking.BL.Interfaces.IParkingService;

namespace CoolParking.WebAPI.Services
{
    public class VehiclesService : IVehiclesService
    {
        private readonly IParkingService _parkingService;

        public ReadOnlyCollection<Vehicle> Vehicles()
        {
            var vehicles = _parkingService.GetVehicles();

            return vehicles;
        }

        public VehicleRequest VehicleById(string id)
        {
            CheckValidId(id);

            try
            {
                var vehicle = Vehicle.GetVehicleById(id);

                if (vehicle == null) throw new NotFoundException($"Didn't found vehicle by id - {id}.");

                var vehicleType = (int)vehicle.VehicleType;
                var vehicleReq = new VehicleRequest(vehicle.Id, vehicleType, vehicle.Balance);

                return vehicleReq;
            }

            catch (Exception)
            {
                throw new NotFoundException($"Didn't found vehicle by id - {id}.");
            }
        }

        public VehicleRequest AddVehicle(VehicleRequest vehicleRequest)
        {
            try
            {
                if (vehicleRequest == null || vehicleRequest.Balance <= 0) throw new BadRequestException("Not valid vehicle model!");

                CheckValidId(vehicleRequest.Id);

                var vehicleType = (VehicleType)vehicleRequest.VehicleType;
                var vehicle = new Vehicle(vehicleRequest.Id, vehicleType, vehicleRequest.Balance);
                _parkingService.AddVehicle(vehicle);

                return vehicleRequest;
            }
            catch (Exception)
            {
                throw new BadRequestException("Not valid!");
            }
        }

        public void RemoveVehicleById(string id)
        {
            CheckValidId(id);

            try
            {
                _parkingService.RemoveVehicle(id);
                return;
            }

            catch (Exception)
            {
                throw new NotFoundException($"Didn't found vehicle by id - {id}.");
            }
        }

        private void CheckValidId(string id)
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");

            if (!regex.IsMatch(id)) throw new BadRequestException($"Your id - {id} isn't valid!");
            return;
        }
    }
}
