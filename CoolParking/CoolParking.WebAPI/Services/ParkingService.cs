﻿using System.Net.Http;
using IParkingService = CoolParking.WebAPI.Interfaces.IParkingService;

namespace CoolParking.WebAPI.Services
{
    public class ParkingService : IParkingService
    {
        private HttpClient _client;

        private readonly IParkingService _parkingService;

        public ParkingService()
        {
            _client = new HttpClient();
        }

        public decimal Balance()
        {
            var balance = _parkingService.Balance();
            return balance;
        }

        public int Capacity()
        {
            var settingsBL = BL.Models.Settings.ParkingSpace;
            return settingsBL;
        }

        public int FreePlaces()
        {
            var freePlaces = _parkingService.FreePlaces();
            return freePlaces;
        }
    }
}
