﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Exceptions;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using System;
using System.Collections.Generic;
using IParkingService = CoolParking.BL.Interfaces.IParkingService;

namespace CoolParking.WebAPI.Services
{
    public class TransactionsService : ITransactionsService
    {
        private readonly IParkingService _parkingService;

        private readonly string logFilePath;

        public List<TransactionInfo> LastTransactionInfo()
        {
            var transaction = _parkingService.GetLastParkingTransactions();

            if (transaction.Length == 0) return new List<TransactionInfo>();

            var lastTransaction = transaction[^1];
            var listTransactions = new List<TransactionInfo>() { lastTransaction };
            return listTransactions;
        }

        public string AllTransactions()
        {
            var logService = new BL.Services.LogService(logFilePath);
            
            try
            {
                var logs = logService.Read();
                return logs;
            }
            catch(Exception)
            {
                throw new NotFoundException("Log file didn't find");
            }
        }

        public Vehicle TopUpVehicle(ToUpVehicleRequest toUpVehicleRequest)
        {
            if (toUpVehicleRequest == null || toUpVehicleRequest.Valid() == false) throw new BadRequestException("Invalid request");
            
            try
            {
                _parkingService.TopUpVehicle(toUpVehicleRequest.Id, toUpVehicleRequest.Sum);

                var vehicle = Vehicle.GetVehicleById(toUpVehicleRequest.Id);

                if (vehicle == null) throw new NotFoundException($"Didn't found vehicle by id - {toUpVehicleRequest.Id}.");

                return vehicle;
            }

            catch (Exception)
            {
                throw new NotFoundException($"Didn't found vehicle by id - {toUpVehicleRequest.Id}.");
            }
        }
    }
}
