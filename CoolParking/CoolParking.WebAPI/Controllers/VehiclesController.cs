﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IVehiclesService vehiclesService;

        public VehiclesController(IVehiclesService service)
        {
            vehiclesService = service;
        }

        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> Vehicles()
        {
            var vehicles = vehiclesService.Vehicles();
            var jsonVehicles = JsonConvert.SerializeObject(vehicles);
            return Ok(jsonVehicles);
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleRequest> VehicleById(string id)
        {
            var vehicle = vehiclesService.VehicleById(id);
            var jsonVehicle = JsonConvert.SerializeObject(vehicle);
            return Ok(jsonVehicle);
        }

        [HttpPost]
        public ActionResult<VehicleRequest> AddVehicle(VehicleRequest vehicleRequest)
        {
            var vehicle = vehiclesService.AddVehicle(vehicleRequest);
            var jsonVehicle = JsonConvert.SerializeObject(vehicle);
            return StatusCode(201, jsonVehicle);
            //return CreatedAtRoute("vehicle", jsonVehicle); 
        }

        [HttpDelete("{id}")]
        public ActionResult RemoveVehicleById(string id)
        {
            vehiclesService.RemoveVehicleById(id);
            return NoContent();
        }
    }
}
