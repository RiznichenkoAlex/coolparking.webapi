﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private ITransactionsService transactionsService;

        public TransactionsController(ITransactionsService service)
        {
            transactionsService = service;
        }

        [HttpGet("last")]
        public ActionResult<List<TransactionInfo>> Last()
        {
            var transact = transactionsService.LastTransactionInfo();
            var jsonTransation = JsonConvert.SerializeObject(transact);
            return Ok(jsonTransation);
        }

        [HttpGet("all")]
        public ActionResult<string> All()
        {
            var allTransactions = transactionsService.AllTransactions();
            return Ok(allTransactions);
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] ToUpVehicleRequest toUpVehicleRequest)
        {
            var vehicle = transactionsService.TopUpVehicle(toUpVehicleRequest);
            var jsonVehicle = JsonConvert.SerializeObject(vehicle);
            return Ok(jsonVehicle);
        }
    }
}
