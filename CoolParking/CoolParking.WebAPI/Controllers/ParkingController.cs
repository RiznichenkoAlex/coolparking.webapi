﻿using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;

        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }

        [HttpGet("balance")]
        public ActionResult<decimal> Balance()
        {
            var balance = parkingService.Balance();
            return Ok(balance);
        }

        [HttpGet("capacity")]
        public ActionResult<int> Capacity()
        {
            var capacity = parkingService.Capacity();
            return Ok(capacity);
        }

        [HttpGet("freeplaces")]
        public ActionResult<int> FreePlaces()
        {
            var freePlaces = parkingService.FreePlaces();
            return Ok(freePlaces);
        }
    }
}
