﻿using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class VehicleRequest
    {
        [JsonProperty("id")]
        public string Id { get; private set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; private set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public VehicleRequest(string id, int vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
    }
}
