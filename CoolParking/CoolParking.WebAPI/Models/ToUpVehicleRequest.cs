﻿using Newtonsoft.Json;
using System.Text.RegularExpressions;
using static System.String;

namespace CoolParking.WebAPI.Models
{
    public class ToUpVehicleRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Sum")]
        public decimal Sum { get; set; }

        public ToUpVehicleRequest() { }

        public bool Valid()
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");

            if (!regex.IsMatch(Id)) return false;
            if (!IsNullOrEmpty(Id) && (Sum > 0)) return true;
            return false;
        }
    }
}
