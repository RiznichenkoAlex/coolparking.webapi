﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;
using static System.String;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get { return _logFilePath; } }

        private readonly string _logFilePath;

        public LogService(string logFilePath)
        {
            _logFilePath = logFilePath ?? throw new ArgumentNullException("LogFilePath cann't be null", nameof(logFilePath));
        }

        public string Read()
        {
            using (StreamReader reader = new StreamReader(_logFilePath))
            {
                var log = reader.ReadToEnd();
                return log;
            }
        }

        public void Write(string logInfo)
        {
            if (IsNullOrEmpty(logInfo)) throw new ArgumentNullException(nameof(logInfo));

            using var writer = new StreamWriter(_logFilePath, true);
            writer.WriteLine(logInfo);
        }
    }
}